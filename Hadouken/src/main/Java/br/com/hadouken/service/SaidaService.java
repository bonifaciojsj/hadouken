package br.com.hadouken.service;

import br.com.hadouken.control.ProdutoControl;
import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.*;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.exception.ProdutoAbaixoEstoqueMinimoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class SaidaService implements Serializable {

    @Inject
    private DAO<Saida, Long> saidaDAO;
    @Inject
    private DAO<SaidaProduto, SaidaProdutoPK> saidaProdutoDAO;
    @Inject
    private DAO<Produto, Long> produtoDAO;
    @Inject
    private ProdutoControl produtoControl;

    public Saida salvar(Saida saida) throws HadoukenException {
        try {
            List<SaidaProduto> produtos = saida.getSaidaProduto();
            List<SaidaProduto> novosProdutos = new ArrayList<>();
            saida.setSaidaProduto(null);
            saida = saidaDAO.save(saida);
            for (SaidaProduto saidaProduto : produtos) {
                Integer quantidade = saidaProduto.getQuantidade();
                Produto produto = saidaProduto.getProduto();
                if (saida.getStatus().equals(StatusTransacao.FINALIZADO)) {
                    produto.setEstoque(produto.getEstoque() - quantidade);
                    produto = produtoDAO.save(produto);
                }
                SaidaProdutoPK pk = new SaidaProdutoPK(saida, produto);
                saidaProduto = new SaidaProduto();
                saidaProduto.setId(pk);
                saidaProduto.setSaida(pk.getSaida());
                saidaProduto.setProduto(pk.getProduto());
                saidaProduto.setQuantidade(quantidade);
                novosProdutos.add(saidaProduto);
            }
            saida.setSaidaProduto(novosProdutos);
            return saidaDAO.save(saida);
        } catch (ConstraintViolationException ex) {
            throw new HadoukenException("Campos obrigatórios não preenchidos");
        }
    }

    public void verificarSeProdutosFicaramAbaixoDoEstoqueMinimoPorSaida(Saida saida) throws ProdutoAbaixoEstoqueMinimoException {
        if (saida.getStatus().equals(StatusTransacao.FINALIZADO)) {
            List<Produto> produtos = new ArrayList<>();
            for (SaidaProduto sp : saida.getSaidaProduto()) {
                produtos.add(sp.getProduto());
            }
            produtoControl.verificarSeEstoquesFicaramAbaixoDoMinimo(produtos);
        }
    }

    public Saida buscarPorId(Long id) {
        return saidaDAO.findByPrimaryKey(id);
    }

    public List<Saida> buscarTodas() {
        String query = "SELECT saida FROM " + Saida.class.getSimpleName() + " saida"
                + " WHERE saida.origem = " + OrigemTransacao.NORMAL.getIndex();
        return saidaDAO.findByQuery(query);
    }

    public List<Saida> buscarTodasGroupByOrigem() {
        String query = "SELECT saida FROM " + Saida.class.getSimpleName() + " saida"
                + " GROUP BY saida.origem";
        return saidaDAO.findByQuery(query);
    }
    
    public List<SaidaProduto> buscarSaidaProdutoGroupByProduto() {
        String query = "SELECT saidaProduto FROM " + SaidaProduto.class.getSimpleName() + " saidaProduto"
                + " GROUP BY saidaProduto.produto";
        return saidaProdutoDAO.findByQuery(query);
    }
    
    public Saida buscarPorIdFetchSaidaProduto(Long id) {
        String query = "SELECT saida FROM " + Saida.class.getSimpleName() + " saida"
                + " LEFT JOIN FETCH saida.saidaProduto"
                + " WHERE saida.id = " + id;
        List<Saida> saidas = saidaDAO.findByQuery(query);
        if (!saidas.isEmpty()) {
            return saidas.get(0);
        }
        return null;
    }

    public List<Saida> buscarTodasFetchSaidaProdutoPorStatusTransacao(StatusTransacao status) {
        String query = "SELECT saida FROM " + Saida.class.getSimpleName() + " saida"
                + " LEFT JOIN FETCH saida.saidaProduto"
                + " WHERE saida.status = " + status.getIndex()
                + " AND saida.origem = " + OrigemTransacao.NORMAL.getIndex();
        return saidaDAO.findByQuery(query);
    }

    public List<Saida> buscarTodasFinalizadasFetchSaidaProduto() {
        return buscarTodasFetchSaidaProdutoPorStatusTransacao(StatusTransacao.FINALIZADO);
    }

    public List<Saida> buscarListagem(Map<String, Object> filtros) {
        StatusTransacao[] statusFiltros = (StatusTransacao[]) filtros.get("status");
        String filtroStatus = "";
        for (StatusTransacao status : statusFiltros) {
            if (!filtroStatus.isEmpty()) {
                filtroStatus += ", ";
            } 
            filtroStatus += String.valueOf(status.getIndex());
        }
        String query = "SELECT saida FROM " + Saida.class.getSimpleName() + " saida"
                + " LEFT JOIN saida.saidaProduto saidaProduto"
                + " WHERE (saida.cliente.nome LIKE '%" + filtros.get("filtroNomeCliente") + "%'"
                + "     OR saida.cliente.sobrenome LIKE '% " + filtros.get("filtroNomeCliente") + "%')"
                + " AND saidaProduto.produto.nome LIKE '%" + filtros.get("produtoNome") + "%'"
                + " AND saidaProduto.produto.codigoBarras LIKE '%" + filtros.get("codigoBarras") + "%'"
                + (filtroStatus.isEmpty() ? "" : " AND saida.status IN (" + filtroStatus + ")")
                + " AND saida.origem = " + OrigemTransacao.NORMAL.getIndex();
        return saidaDAO.findByQuery(query);
    }

}
