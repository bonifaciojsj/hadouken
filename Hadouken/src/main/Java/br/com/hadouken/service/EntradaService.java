package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.*;
import br.com.hadouken.exception.HadoukenException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class EntradaService implements Serializable {
    
    @Inject
    private DAO<Entrada, Long> entradaDAO;
    @Inject
    private DAO<EntradaProduto, EntradaProdutoPK> entradaProdutoDAO;
    @Inject
    private DAO<Produto, Long> produtoDAO;
    
    public Entrada salvar(Entrada entrada) throws HadoukenException {
        try {
            List<EntradaProduto> produtos = entrada.getEntradaProduto();
            List<EntradaProduto> novosProdutos = new ArrayList<>();
            entrada.setEntradaProduto(null);
            entrada = entradaDAO.save(entrada);
            for (EntradaProduto entradaProduto : produtos) {
                Integer quantidade = entradaProduto.getQuantidade();
                Produto produto = entradaProduto.getProduto();
                if (entrada.getStatus().equals(StatusTransacao.FINALIZADO)) {
                    produto.setEstoque(produto.getEstoque() + quantidade);
                    produto = produtoDAO.save(produto);
                }
                EntradaProdutoPK pk = new EntradaProdutoPK(entrada, produto);
                entradaProduto = new EntradaProduto();
                entradaProduto.setId(pk);
                entradaProduto.setEntrada(pk.getEntrada());
                entradaProduto.setProduto(pk.getProduto());
                entradaProduto.setQuantidade(quantidade);            
                novosProdutos.add(entradaProduto);
            }
            entrada.setEntradaProduto(novosProdutos);
            return entradaDAO.save(entrada);
        } catch (ConstraintViolationException ex) {
            throw new HadoukenException("Campos obrigatórios não preenchidos");
        }
    }
    
    public Entrada buscarPorId(Long id) {
        return entradaDAO.findByPrimaryKey(id);
    }
    
    public List<Entrada> buscarTodas() {
        String query = "SELECT entrada FROM " + Entrada.class.getSimpleName() + " entrada"
                + " WHERE entrada.origem = " + OrigemTransacao.NORMAL.getIndex();
        return entradaDAO.findByQuery(query);
    }
    
    public Entrada buscarPorIdFetchEntradaProduto(Long id) {
        String query = "SELECT entrada FROM " + Entrada.class.getSimpleName() + " entrada"
                + " LEFT JOIN FETCH entrada.entradaProduto"
                + " WHERE entrada.id = " + id;
        List<Entrada> entradas = entradaDAO.findByQuery(query);
        if (!entradas.isEmpty()) {
            return entradas.get(0);
        } 
        return null;
    }
    
    public List<EntradaProduto> buscarEntradaProdutoGroupByProduto() {
        String query = "SELECT entradaProduto FROM " + EntradaProduto.class.getSimpleName() + " entradaProduto"
                + " GROUP BY entradaProduto.produto";
        return entradaProdutoDAO.findByQuery(query);
    }
    
    
    public List<Entrada> buscarListagem(Map<String, Object> filtros) {
        StatusTransacao[] statusFiltros = (StatusTransacao[]) filtros.get("status");
        String filtroStatus = "";
        for (StatusTransacao status : statusFiltros) {
            if (!filtroStatus.isEmpty()) {
                filtroStatus += ", ";
            } 
            filtroStatus += String.valueOf(status.getIndex());
        }
        String query = "SELECT entrada FROM " + Entrada.class.getSimpleName() + " entrada"
                + " LEFT JOIN entrada.entradaProduto entradaProduto"
                + " WHERE entrada.fornecedor.razaoSocial LIKE '%" + filtros.get("fornecedorRazaoSocial") + "%'"
                + " AND entradaProduto.produto.nome LIKE '%" + filtros.get("produtoNome") + "%'"
                + " AND entradaProduto.produto.codigoBarras LIKE '%" + filtros.get("codigoBarras") + "%'"
                + (filtroStatus.isEmpty() ? "" : " AND entrada.status IN (" + filtroStatus + ")")
                + " AND entrada.origem = " + OrigemTransacao.NORMAL.getIndex();
        return entradaDAO.findByQuery(query);
    }
    
}
