package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.GrupoProduto;
import br.com.hadouken.entity.Produto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class ProdutoService implements Serializable {
    
    @Inject
    private DAO<Produto, Long> produtoDAO;
    
    public Produto salvar(Produto produto) {
        return produtoDAO.save(produto);
    }
    
    public Produto buscarPorId(Long id) {
        return produtoDAO.findByPrimaryKey(id);
    }
    
    public List<Produto> buscarTodos() {
        return produtoDAO.findAll();
    }
    
    public List<Produto> buscarAtivos() {
        String query = "SELECT produto FROM " + Produto.class.getSimpleName() + " produto"
                + " WHERE produto.ativo = true";
        return produtoDAO.findByQuery(query);        
    }
    
    public List<Produto> buscarAtivosComEstoqueAbaixoDoMinimo() {
        String query = "SELECT produto FROM " + Produto.class.getSimpleName() + " produto"
                + " WHERE produto.ativo = true"
                + " AND produto.estoque < produto.estoqueMinimo";
        return produtoDAO.findByQuery(query);        
    }
    
    public List<Produto> buscarAtivosComEstoqueIgualOuAbaixoDoMinimo() {
        String query = "SELECT produto FROM " + Produto.class.getSimpleName() + " produto"
                + " WHERE produto.ativo = true"
                + " AND produto.estoque <= produto.estoqueMinimo"
                + " ORDER BY produto.grupo";
        return produtoDAO.findByQuery(query);        
    }
    
    private List<Produto> buscarAtivosPorGrupo(GrupoProduto grupo) {
        String query = "SELECT produto FROM " + Produto.class.getSimpleName() + " produto"
                + " WHERE produto.ativo = true"
                + " AND produto.grupo = " + grupo.getIndex();
        return produtoDAO.findByQuery(query);        
    }
    
    public List<Produto> buscarAtivosMateriaPrima() {
        return buscarAtivosPorGrupo(GrupoProduto.MATERIAPRIMA);
    }
    
    public List<Produto> buscarAtivosArtesanal() {
        return buscarAtivosPorGrupo(GrupoProduto.ARTESANAL);        
    }
    
    public List<Produto> buscarAtivosIndustrializados() {
        return buscarAtivosPorGrupo(GrupoProduto.INDUSTRIALIZADO);        
    }
    
    public List<Produto> buscarAtivosNaoArtesanais() {
        List<Produto> produtos = new ArrayList<>();
        produtos.addAll(buscarAtivosMateriaPrima());
        produtos.addAll(buscarAtivosIndustrializados());
        return produtos;        
    }
    
    public List<Produto> buscarAtivosComercializaveis() {
        List<Produto> produtos = new ArrayList<>();
        produtos.addAll(buscarAtivosArtesanal());
        produtos.addAll(buscarAtivosIndustrializados());
        return produtos;
    }
    
    public List<Produto> buscarListagem(Map<String, Object> filtros) {
        GrupoProduto[] grupos = (GrupoProduto[]) filtros.get("grupos");
        String filtroGrupos = "";
        for (GrupoProduto grupo : grupos) {
            if (!filtroGrupos.isEmpty()) {
                filtroGrupos += ", ";
            } 
            filtroGrupos += String.valueOf(grupo.getIndex());
        }
        String query = "SELECT produto FROM " + Produto.class.getSimpleName() + " produto"
                + " WHERE produto.nome LIKE '%" + filtros.get("nome") + "%'"
                + (filtroGrupos.isEmpty() ? "" : " AND produto.grupo IN (" + filtroGrupos + ")")
                + " AND produto.codigoBarras LIKE '%" + filtros.get("codigoBarras") + "%'"
                + " AND ((produto.estoque < produto.estoqueMinimo AND true = "
                + filtros.get("abaixoEstoqueMinimo") + ") OR false = " + filtros.get("abaixoEstoqueMinimo") + ")"
                + " AND produto.ativo = " + filtros.get("ativo");
        return produtoDAO.findByQuery(query);
    }
    
}
