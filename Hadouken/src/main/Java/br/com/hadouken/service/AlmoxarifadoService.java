package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.Almoxarifado;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author u0181089
 */
@Stateless
public class AlmoxarifadoService implements Serializable {
    
    @Inject
    private DAO<Almoxarifado, Long> almoxarifadoDAO;
    
    public Almoxarifado salvar(Almoxarifado almoxarifado) {
        return almoxarifadoDAO.save(almoxarifado);
    }
    
    public Almoxarifado buscarPorId(Long id) {
        return almoxarifadoDAO.findByPrimaryKey(id);
    }

    public List<Almoxarifado> buscarTodos() {
        return almoxarifadoDAO.findAll();
    }
    
    public List<Almoxarifado> buscarTodosAtivos() {
        String query = "SELECT almoxarifado FROM " + Almoxarifado.class.getSimpleName() + " almoxarifado"
                + " WHERE almoxarifado.ativo = true";
        return almoxarifadoDAO.findByQuery(query);
    }
}
