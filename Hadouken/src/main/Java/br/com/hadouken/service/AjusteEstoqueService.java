package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.AjusteEstoque;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author u0181089
 */
@Stateless
public class AjusteEstoqueService implements Serializable {

    @Inject
    private DAO<AjusteEstoque, Long> ajusteEstoqueDAO;

    public AjusteEstoque salvar(AjusteEstoque ajusteEstoque) {
        return ajusteEstoqueDAO.save(ajusteEstoque);
    }

    public AjusteEstoque buscarPorId(Long id) {
        return ajusteEstoqueDAO.findByPrimaryKey(id);
    }

    public AjusteEstoque buscarPorIdFetchEntradaESaidaDeProdutos(Long id) {
        String query = "SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                + " WHERE ajusteEstoque = " + id;
        List<AjusteEstoque> ajustes = ajusteEstoqueDAO.findByQuery(query);
        if (!ajustes.isEmpty()) {
            AjusteEstoque ajuste = ajustes.get(0);
            ajuste = ajusteEstoqueDAO.findByQuery("SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                    + " LEFT JOIN FETCH ajusteEstoque.entrada entrada"
                    + " LEFT JOIN FETCH entrada.entradaProduto"
                    + " WHERE ajusteEstoque = " + ajuste.getId()).get(0);
            ajuste = ajusteEstoqueDAO.findByQuery("SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                    + " LEFT JOIN FETCH ajusteEstoque.saida saida"
                    + " LEFT JOIN FETCH saida.saidaProduto"
                    + " WHERE ajusteEstoque = " + ajuste.getId()).get(0);
            return ajuste;
        }
        return null;
    }

    public List<AjusteEstoque> buscarTodos() {
        return ajusteEstoqueDAO.findAll();
    }
    
    public List<AjusteEstoque> buscarListagem(Map<String, Object> filtros) {
        String query = "SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                + " LEFT JOIN ajusteEstoque.saida.saidaProduto saidaProduto"
                + " LEFT JOIN ajusteEstoque.entrada.entradaProduto entradaProduto"
                + " WHERE (entradaProduto.produto.nome LIKE '%" + filtros.get("produtoNome") + "%'"
                + "     OR saidaProduto.produto.nome LIKE '%" + filtros.get("produtoNome") + "%')"
                + " AND (entradaProduto.produto.codigoBarras LIKE '%" + filtros.get("codigoBarras")  + "%'"
                + "     OR saidaProduto.produto.codigoBarras LIKE '%" + filtros.get("codigoBarras") + "%')"
                + " AND (ajusteEstoque.saida.usuarioCadastro.nome LIKE '%" + filtros.get("usuarioResponsavel") + "%'"
                + "     OR ajusteEstoque.saida.usuarioCadastro.sobrenome LIKE '%" + filtros.get("usuarioResponsavel")+ "%'"
                + "     OR ajusteEstoque.entrada.usuarioCadastro.nome LIKE '% " + filtros.get("usuarioResponsavel") + "%'"
                + "     OR ajusteEstoque.entrada.usuarioCadastro.sobrenome LIKE '%" + filtros.get("usuarioResponsavel") + "%')"
                + " AND (ajusteEstoque.finalizado = " + filtros.get("ajusteFinalizado")
                + "     OR ajusteEstoque.finalizado = " + filtros.get("ajusteFinalizado") + ")";
        return ajusteEstoqueDAO.findByQuery(query);
    }

    public List<AjusteEstoque> fetchEntradaESaidaDeProdutos(List<AjusteEstoque> ajustes) {
        if (!ajustes.isEmpty()) {
            String ajustesId = "";
            for (AjusteEstoque ajuste : ajustes) {
                if (!ajustesId.isEmpty()) {
                    ajustesId += ", ";
                }
                ajustesId += String.valueOf(ajuste.getId());
            }
            ajustes = ajusteEstoqueDAO.findByQuery("SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                    + " LEFT JOIN FETCH ajusteEstoque.entrada entrada"
                    + " LEFT JOIN FETCH entrada.entradaProduto"
                    + " WHERE ajusteEstoque IN (" + ajustesId + ")");
            ajustes = ajusteEstoqueDAO.findByQuery("SELECT ajusteEstoque FROM " + AjusteEstoque.class.getSimpleName() + " ajusteEstoque"
                    + " LEFT JOIN FETCH ajusteEstoque.saida saida"
                    + " LEFT JOIN FETCH saida.saidaProduto"
                    + " WHERE ajusteEstoque IN (" + ajustesId + ")");
            return ajustes;
        }
        return new ArrayList<>();
    }

}
