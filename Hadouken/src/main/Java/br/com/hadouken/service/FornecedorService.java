package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.Fornecedor;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class FornecedorService implements Serializable {
    
    @Inject
    private DAO<Fornecedor, Long> fornecedorDAO;
    
    public Fornecedor salvar(Fornecedor fornecedor) {
        return fornecedorDAO.save(fornecedor);
    }
    
    public Fornecedor buscarPorId(Long id) {
        return fornecedorDAO.findByPrimaryKey(id);
    }
    
    public List<Fornecedor> buscarTodos() {
        return fornecedorDAO.findAll();
    }
    
    public List<Fornecedor> buscarAtivos() {        
        String query = "SELECT fornecedor FROM " + Fornecedor.class.getSimpleName() + " fornecedor"
                + " WHERE fornecedor.ativo = true";
        return fornecedorDAO.findByQuery(query);
    }
    
    public List<Fornecedor> buscarListagem(Map<String, Object> filtros) {
        String query = "SELECT fornecedor FROM " + Fornecedor.class.getSimpleName() + " fornecedor"
                + " WHERE fornecedor.razaoSocial LIKE '%" + filtros.get("razaoSocial") + "%'"
                + " AND fornecedor.cnpj LIKE '%" + filtros.get("cnpj") + "%'"
                + " AND fornecedor.email LIKE '%" + filtros.get("email") + "%'"
                + " AND fornecedor.fone LIKE '%" + filtros.get("telefone") + "%'"
                + " AND fornecedor.ativo = " + filtros.get("ativo");
        return fornecedorDAO.findByQuery(query);
    }
}
