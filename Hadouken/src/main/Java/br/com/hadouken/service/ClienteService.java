package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.Cliente;
import br.com.hadouken.entity.TipoPessoa;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author jose.bonifacio
 */
@Stateless
public class ClienteService implements Serializable {    
    
    @Inject
    private DAO<Cliente, Long> clienteDAO;
    
    public Cliente salvar(Cliente cliente) {
        return clienteDAO.save(cliente);
    }
    
    public Cliente buscarPorId(Long id) {
        return clienteDAO.findByPrimaryKey(id);
    }
    
    public List<Cliente> buscarTodos() {
        return clienteDAO.findAll();
    }
    
    public List<Cliente> buscarListagem(Map<String, Object> filtros) {
        String query = "SELECT cliente FROM " + Cliente.class.getSimpleName() + " cliente"
                + " WHERE (cliente.nome LIKE '%" + filtros.get("nome") + "%' OR cliente.sobrenome LIKE '%" + filtros.get("nome") + "%')"
                + " AND cliente.tipoPessoa = " + ((TipoPessoa)filtros.get("tipoPessoa")).getIndex()
                + " AND (cliente.cpf LIKE '%" + filtros.get("cpfCnpj") + "%' OR cliente.cnpj LIKE '%" + filtros.get("cpfCnpj") + "%')"
                + " AND cliente.email LIKE '%" + filtros.get("email") + "%'"
                + " AND (cliente.fone LIKE '%" + filtros.get("foneCelular") + "%' OR cliente.celular LIKE '%" + filtros.get("foneCelular") + "%')"
                + " AND cliente.ativo = " + filtros.get("ativo");
        return clienteDAO.findByQuery(query);
    }
}
