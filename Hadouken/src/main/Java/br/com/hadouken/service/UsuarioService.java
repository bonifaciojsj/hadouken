package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.Usuario;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.util.PasswordUtil;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author BONIFACIO
 */
@Stateless
public class UsuarioService implements Serializable {

    @Inject
    private DAO<Usuario, Long> usuarioDAO;

    
    public Usuario salvar(Usuario usuario) {
        return usuarioDAO.save(usuario);
    }
    
    public Usuario salvarNovoUsuario(Usuario usuario, String senhaConfirmacao) throws HadoukenException {
        if (usuario.isNew()) {
            validarSenhaIgual(usuario.getPassword(), senhaConfirmacao);  
        }
        return salvar(usuario);
    }
    
    public Usuario salvarUsuarioComNovaSenha(Usuario usuario,
            String senhaAntiga,
            String novaSenha,
            String novaSenhaConfirmacao) throws HadoukenException {
        validarSenhaAntiga(usuario, senhaAntiga);
        validarSenhaIgual(novaSenha, novaSenhaConfirmacao);
        usuario.setPasswordCriptografada(novaSenha);
        return salvar(usuario);
    }

    public Usuario buscarPorId(Long id) {
        return usuarioDAO.findByPrimaryKey(id);
    }

    public List<Usuario> buscarTodos() {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName()
                + " usuario WHERE usuario.id <> 1L";
        return usuarioDAO.findByQuery(query);
    }

    public Usuario buscarAtivosPorUsuarioESenha(String username, String password) {
        String query = "SELECT usuario FROM " + Usuario.class.getSimpleName() 
                + " usuario WHERE usuario.username = '" + username 
                + "' AND usuario.password = '" + PasswordUtil.getEncryptedPassword(password) + "'"
                + " AND usuario.ativo = true";
        List<Usuario> usuarios = usuarioDAO.findByQuery(query);
        if (!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }
    
    public void validarSenhaIgual(String senha1, String senha2) throws HadoukenException {
        if (!senha1.equals(senha2)) {
            throw new HadoukenException("A nova senha informada não coincide com sua confirmação.");
        }        
    }
    
    public void validarSenhaAntiga(Usuario usuario, String senha) throws HadoukenException {
        if (!usuario.getPassword().equals(senha)) {
            throw new HadoukenException("A senha informada não coincide com a anterior.");
        } 
    }
}
