package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.SubtipoProduto;
import br.com.hadouken.entity.TipoProduto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author u0181089
 */
@Stateless
public class SubtipoProdutoService implements Serializable {
    
    @Inject
    private DAO<SubtipoProduto, Long> tipoProdutoDAO;
    
    public SubtipoProduto salvar(SubtipoProduto tipoProduto) {
        return tipoProdutoDAO.save(tipoProduto);
    }
    
    public SubtipoProduto buscarPorId(Long id) {
        return tipoProdutoDAO.findByPrimaryKey(id);
    }

    public List<SubtipoProduto> buscarTodos() {
        return tipoProdutoDAO.findAll();
    }
    
    public List<SubtipoProduto> buscarTodosAtivos() {
        String query = "SELECT subtipoProduto FROM " + SubtipoProduto.class.getSimpleName() + " subtipoProduto"
                + " WHERE subtipoProduto.ativo = true";
        return tipoProdutoDAO.findByQuery(query);
    }
    
    public List<SubtipoProduto> buscarTodosAtivosPorTipo(TipoProduto tipo) {
        if (!tipo.isNew()) {
            String query = "SELECT subtipoProduto FROM " + SubtipoProduto.class.getSimpleName() + " subtipoProduto"
                + " WHERE subtipoProduto.ativo = true"
                + " AND subtipoProduto.tipoProduto.id = " + tipo.getId();
            return tipoProdutoDAO.findByQuery(query);            
        }
        return new ArrayList<>();
    }
}
