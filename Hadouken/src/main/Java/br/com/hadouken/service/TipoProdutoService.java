package br.com.hadouken.service;

import br.com.hadouken.dao.DAO;
import br.com.hadouken.entity.TipoProduto;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author u0181089
 */
@Stateless
public class TipoProdutoService implements Serializable {
    
    @Inject
    private DAO<TipoProduto, Long> tipoProdutoDAO;
    
    public TipoProduto salvar(TipoProduto tipoProduto) {
        return tipoProdutoDAO.save(tipoProduto);
    }
    
    public TipoProduto buscarPorId(Long id) {
        return tipoProdutoDAO.findByPrimaryKey(id);
    }

    public List<TipoProduto> buscarTodos() {
        return tipoProdutoDAO.findAll();
    }
    
    public List<TipoProduto> buscarTodosAtivos() {
        String query = "SELECT tipoProduto FROM " + TipoProduto.class.getSimpleName() + " tipoProduto"
                + " WHERE tipoProduto.ativo = true";
        return tipoProdutoDAO.findByQuery(query);
    }
}
