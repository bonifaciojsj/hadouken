package br.com.hadouken.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author jose.bonifacio
 */
public class DateUtil {
    
    private final static DateFormat DATE_PATTERN = new SimpleDateFormat("dd/MM/yyyy");
    
    public static String getFormattedDate(Date data) {
        return DATE_PATTERN.format(data);
    }
}
