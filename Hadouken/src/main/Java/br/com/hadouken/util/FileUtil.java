package br.com.hadouken.util;

import br.com.hadouken.entity.PersistentEntity;
import br.com.hadouken.entity.Usuario;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author jose.bonifacio
 */
public class FileUtil {

    private static final String FILES_DISK_PATH = "C:/Hadouken/Files/";
    private static final String REPORTS_DISK_PATH = "C:/Hadouken/Project/Hadouken/src/main/resources/Relatorio/";

    public static <E extends PersistentEntity> void gerarRelatorio(List<E> source, Usuario usuario, String reportFileName) {
        try {
            JasperPrint jasperPrint;
            JasperReport jasperReport = (JasperReport) JasperCompileManager.compileReport(REPORTS_DISK_PATH.concat(reportFileName.concat(".jrxml")));
            Map<String, Object> parameters = new HashMap<>();

            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(source));

            String folder = FILES_DISK_PATH.concat(usuario.getId().toString());
            checkIfFolderExists(folder);
            JasperExportManager.exportReportToPdfFile(
                    jasperPrint, folder.concat("/").concat(reportFileName.trim()).concat(".pdf"));
        } catch (JRException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void saveFile() {
        File file = new File(FILES_DISK_PATH + "text.txt");
        String content = "This is the text content";

        try (FileOutputStream fop = new FileOutputStream(file)) {
            if (!file.exists()) {
                file.createNewFile();
            }
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void checkIfFolderExists(String folderDirectory) {
        File file = new File(folderDirectory);
        if (!file.exists()) {
            file.mkdir();
        }
    }
}
