package br.com.hadouken.session;

import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BONIFACIO
 *
 */
public class SessionFilter implements Filter {

    @Inject
    private SessionController session;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String contextPath = ((HttpServletRequest) request).getContextPath();
        if (!session.isLoggedIn()) {
            if (!((HttpServletRequest) request).getPathInfo().equals("/webpage/login/login.xhtml")) {
                ((HttpServletResponse) response).sendRedirect(contextPath + "/faces/webpage/login/login.xhtml");
                return;
            }
        } else {
            if (((HttpServletRequest) request).getPathInfo().equals("/webpage/login/login.xhtml")) {
                ((HttpServletResponse) response).sendRedirect(contextPath + "/faces/webpage/home.xhtml");
                return;
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
