package br.com.hadouken.session;

import br.com.hadouken.entity.Usuario;
import br.com.hadouken.service.UsuarioService;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 * Simple login bean.
 *
 * @author itcuties
 */
@Named
@SessionScoped
public class SessionController implements Serializable {

    @Inject
    private UsuarioService usuarioService;

    private String username;
    private String password;

    private boolean loggedIn;
    private Usuario usuarioLogado;

    public String doLogin() {
        Usuario usuario = usuarioService.buscarAtivosPorUsuarioESenha(username, password);
        if (usuario != null) {
            loggedIn = true;
            setUsuarioLogado(usuario);
            return "/webpage/home.xhtml?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", "Usuário/Senha inválidos."));
        }
        return "/webpage/login/login.xhtml";
    }

    /**
     * Logout operation.
     *
     * @return
     */
    public String doLogout() {
        loggedIn = false;
        setUsuarioLogado(null);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/webpage/home.xhtml?faces-redirect=true";
    }

    // ------------------------------
    // Getters & Setters 
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

}
