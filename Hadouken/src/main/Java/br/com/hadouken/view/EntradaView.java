package br.com.hadouken.view;

import br.com.hadouken.entity.*;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.service.AlmoxarifadoService;
import br.com.hadouken.service.EntradaService;
import br.com.hadouken.service.FornecedorService;
import br.com.hadouken.service.ProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class EntradaView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private EntradaService entradaService;
    @Inject
    private FornecedorService fornecedorService;
    @Inject
    private ProdutoService produtoService;
    @Inject
    private AlmoxarifadoService almoxarifadoService;
    private List<Entrada> entradasConsulta;
    private Entrada entrada;
    private List<Fornecedor> fornecedores;
    private List<Produto> produtos;
    private List<Produto> produtosSelected;
    private List<EntradaProduto> entradaProdutoSelected;

    private String filtroRazaoSocialFornecedor;
    private String filtroProdutoNome;
    private String filtroCodigoBarras;
    private StatusTransacao[] filtroStatusTransacao;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        entrada = new Entrada();
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        entrada = entradaService.buscarPorIdFetchEntradaProduto(entrada.getId());
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        try {
            if (entrada.isNew()) {
                entrada.setUsuarioCadastro(session.getUsuarioLogado());
            }
            entrada = entradaService.salvar(entrada);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
            atualizaListas();
            return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
        } catch (HadoukenException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getLocalizedMessage()));
            return null;
        }
    }

    public String cancelarAlteracoes() {
        if (entrada.isNew()) {
            entrada = new Entrada();
        } else {
            entrada = entradaService.buscarPorIdFetchEntradaProduto(entrada.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("fornecedorRazaoSocial", getFiltroRazaoSocialFornecedor());
        filtros.put("produtoNome", getFiltroProdutoNome());
        filtros.put("codigoBarras", getFiltroCodigoBarras());
        filtros.put("status", getFiltroStatusTransacao());
        entradasConsulta = entradaService.buscarListagem(filtros);
        entrada = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroRazaoSocialFornecedor(null);
        setFiltroProdutoNome(null);
        setFiltroCodigoBarras(null);
        setFiltroStatusTransacao(null);
        entradasConsulta = null;
        entrada= null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<Entrada> getEntradas() {
        if (entradasConsulta != null) {
            return entradasConsulta;
        }
        return entradaService.buscarTodas();
    }
    
    public List<Almoxarifado> getAlmoxarifados() {
        return almoxarifadoService.buscarTodosAtivos();
    } 

    public Entrada getEntrada() {
        if (entrada == null) {
            entrada = new Entrada();
        }
        return entrada;
    }

    public void setEntrada(Entrada entrada) {
        this.entrada = entrada;
    }

    public List<Fornecedor> getFornecedores() {
        if (fornecedores == null) {
            fornecedores = fornecedorService.buscarAtivos();
        }
        return fornecedores;
    }

    public void setFornecedores(List<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

    public List<Produto> getProdutos() {
        if (produtos == null) {
            produtos = produtoService.buscarAtivosNaoArtesanais();
        }
        produtos.removeAll(getEntrada().getProdutos());
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public List<Produto> getProdutosSelected() {
        return produtosSelected;
    }

    public void setProdutosSelected(List<Produto> produtosSelected) {
        this.produtosSelected = produtosSelected;
    }

    public List<EntradaProduto> getEntradaProdutoSelected() {
        return entradaProdutoSelected;
    }

    public void setEntradaProdutoSelected(List<EntradaProduto> entradaProdutoSelected) {
        this.entradaProdutoSelected = entradaProdutoSelected;
    }

    public String getFiltroRazaoSocialFornecedor() {
        return filtroRazaoSocialFornecedor;
    }

    public void setFiltroRazaoSocialFornecedor(String filtroRazaoSocialFornecedor) {
        this.filtroRazaoSocialFornecedor = filtroRazaoSocialFornecedor;
    }

    public StatusTransacao[] getStatusTrancacao() {
        return StatusTransacao.values();
    }

    public boolean entradaFinalizada() {
        return entrada.getStatus().equals(StatusTransacao.FINALIZADO);
    }
    
    public boolean entradaFinalizadaESalva() {
        return entradaFinalizada() && !getEntrada().isNew();
    }
    
    public String getFiltroProdutoNome() {
        return filtroProdutoNome;
    }

    public void setFiltroProdutoNome(String filtroProdutoNome) {
        this.filtroProdutoNome = filtroProdutoNome;
    }

    public String getFiltroCodigoBarras() {
        return filtroCodigoBarras;
    }

    public void setFiltroCodigoBarras(String filtroCodigoBarras) {
        this.filtroCodigoBarras = filtroCodigoBarras;
    }

    public StatusTransacao[] getFiltroStatusTransacao() {
        if (filtroStatusTransacao == null) {
            filtroStatusTransacao = getStatusTrancacao();
        }
        return filtroStatusTransacao;
    }

    public void setFiltroStatusTransacao(StatusTransacao[] filtroStatusTransacao) {
        this.filtroStatusTransacao = filtroStatusTransacao;
    }

    public void removerProdutosSelecionados() {
        for (EntradaProduto ep : getEntradaProdutoSelected()) {
            if (getEntrada().getEntradaProduto().contains(ep)) {
                getEntrada().getEntradaProduto().remove(ep);
            }
        }
        atualizaListas();
    }

    public void adicionarProdutosSelecionados() {
        List<EntradaProduto> ep = new ArrayList<>();
        for (Produto ps : getProdutosSelected()) {
            EntradaProduto entradaProduto = new EntradaProduto();
            entradaProduto.setProduto(ps);
            ep.add(entradaProduto);
        }
        getEntrada().getEntradaProduto().addAll(ep);
        atualizaListas();
    }

    public void atualizaListas() {
        this.entradaProdutoSelected = null;
        this.produtos = null;
        this.produtosSelected = null;
        this.fornecedores = null;
    }
}
