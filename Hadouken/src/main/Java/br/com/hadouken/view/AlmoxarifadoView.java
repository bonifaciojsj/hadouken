package br.com.hadouken.view;

import br.com.hadouken.entity.Almoxarifado;
import br.com.hadouken.service.AlmoxarifadoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author u0181089
 */
@Named
@SessionScoped
public class AlmoxarifadoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private AlmoxarifadoService almoxarifadoService;
    private Almoxarifado almoxarifado;

    public List<Almoxarifado> getAlmoxarifados() {
        return almoxarifadoService.buscarTodos();
    }

    public String voltar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        almoxarifado = new Almoxarifado();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (almoxarifado.isNew()) {
            almoxarifado.setUsuarioCadastro(session.getUsuarioLogado());
        }
        almoxarifado = almoxarifadoService.salvar(almoxarifado);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;

    }

    public String cancelarAlteracoes() {
        if (almoxarifado.isNew()) {
            almoxarifado = new Almoxarifado();
        } else {
            almoxarifado = almoxarifadoService.buscarPorId(almoxarifado.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public Almoxarifado getAlmoxarifado() {
        if (almoxarifado == null) {
            almoxarifado = new Almoxarifado();
        }
        return almoxarifado;
    }

    public void setAlmoxarifado(Almoxarifado almoxarifado) {
        this.almoxarifado = almoxarifado;
    }

}
