package br.com.hadouken.view;

import br.com.hadouken.entity.Cliente;
import br.com.hadouken.entity.Estado;
import br.com.hadouken.entity.TipoPessoa;
import br.com.hadouken.service.ClienteService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class ClienteView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private ClienteService clienteService;
    private List<Cliente> clientesConsulta;
    private Cliente cliente;

    private String filtroNome;
    private TipoPessoa filtroTipoPessoa;
    private String filtroCpfCnpj;
    private String filtroEmail;
    private String filtroFoneCelular;
    private boolean filtroAtivo = true;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        cliente = new Cliente();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (cliente.isNew()) {
            cliente.setUsuarioCadastro(session.getUsuarioLogado());
        }
        cliente = clienteService.salvar(cliente);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String cancelarAlteracoes() {
        if (cliente.isNew()) {
            cliente = new Cliente();
        } else {
            cliente = clienteService.buscarPorId(cliente.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("nome", getFiltroNome());
        filtros.put("tipoPessoa", getFiltroTipoPessoa());
        filtros.put("cpfCnpj", getFiltroCpfCnpj());
        filtros.put("ativo", isFiltroAtivo());
        filtros.put("email", getFiltroEmail());
        filtros.put("foneCelular", getFiltroFoneCelular());
        clientesConsulta = clienteService.buscarListagem(filtros);
        cliente = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroNome(null);
        setFiltroTipoPessoa(null);
        setFiltroCpfCnpj(null);
        setFiltroAtivo(true);
        setFiltroEmail(null);
        setFiltroFoneCelular(null);
        clientesConsulta = null;
        cliente = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<Cliente> getClientes() {
        if (clientesConsulta != null) {
            return clientesConsulta;
        }
        return clienteService.buscarTodos();
    }

    public Cliente getCliente() {
        if (cliente == null) {
            cliente = new Cliente();
        }
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoPessoa[] getTiposPessoa() {
        return TipoPessoa.values();
    }

    public Estado[] getEstados() {
        return Estado.values();
    }

    public boolean isDesabilitarCamposPessoaFisica() {
        return getCliente().getTipoPessoa() == null || !getCliente().getTipoPessoa().equals(TipoPessoa.FISICA);
    }

    public boolean isDesabilitarCamposPessoaJuridica() {
        return getCliente().getTipoPessoa() == null || !getCliente().getTipoPessoa().equals(TipoPessoa.JURIDICA);
    }

    public String getFiltroNome() {
        return filtroNome;
    }

    public void setFiltroNome(String filtroNome) {
        this.filtroNome = filtroNome;
    }

    public String getFiltroCpfCnpj() {
        return filtroCpfCnpj;
    }

    public void setFiltroCpfCnpj(String filtroCpfCnpj) {
        this.filtroCpfCnpj = filtroCpfCnpj;
    }

    public TipoPessoa getFiltroTipoPessoa() {
        if (filtroTipoPessoa == null) {
            filtroTipoPessoa = TipoPessoa.FISICA;
        }
        return filtroTipoPessoa;
    }

    public void setFiltroTipoPessoa(TipoPessoa filtroTipoPessoa) {
        this.filtroTipoPessoa = filtroTipoPessoa;
    }

    public boolean isFiltroAtivo() {
        return filtroAtivo;
    }

    public void setFiltroAtivo(boolean filtroAtivo) {
        this.filtroAtivo = filtroAtivo;
    }

    public String getFiltroEmail() {
        return filtroEmail;
    }

    public void setFiltroEmail(String filtroEmail) {
        this.filtroEmail = filtroEmail;
    }

    public String getFiltroFoneCelular() {
        return filtroFoneCelular;
    }

    public void setFiltroFoneCelular(String filtroFoneCelular) {
        this.filtroFoneCelular = filtroFoneCelular;
    }
    
}
