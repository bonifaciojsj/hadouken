package br.com.hadouken.view;

import br.com.hadouken.entity.Usuario;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.service.UsuarioService;
import br.com.hadouken.util.PasswordUtil;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class UsuarioView implements Serializable {

    @Inject
    private UsuarioService usuarioService;
    private Usuario usuario;
    private String passwordConfirmacao;

    public String voltar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        usuario = new Usuario();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        try {
            if (usuario.isNew()) {
                usuario = usuarioService.salvarNovoUsuario(usuario, getPasswordConfirmacao());
            }
            usuario = usuarioService.salvar(usuario);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
            return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
        } catch (HadoukenException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getMessage()));
            return null;
        }
    }

    public String cancelarAlteracoes() {
        if (usuario.isNew()) {
            usuario = new Usuario();
        } else {
            usuario = usuarioService.buscarPorId(usuario.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public List<Usuario> getUsuarios() {
        return usuarioService.buscarTodos();
    }

    public Usuario getUsuario() {
        if (usuario == null) {
            usuario = new Usuario();
        }
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getPasswordConfirmacao() {
        return passwordConfirmacao;
    }

    public void setPasswordConfirmacao(String passwordConfirmacao) {
        this.passwordConfirmacao = PasswordUtil.getEncryptedPassword(passwordConfirmacao);
    }
}
