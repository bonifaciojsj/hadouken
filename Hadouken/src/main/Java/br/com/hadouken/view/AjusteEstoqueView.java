package br.com.hadouken.view;

import br.com.hadouken.entity.*;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.exception.ProdutoAbaixoEstoqueMinimoException;
import br.com.hadouken.service.*;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author u0181089
 */
@Named
@SessionScoped
public class AjusteEstoqueView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private AjusteEstoqueService ajusteEstoqueService;
    @Inject
    private EntradaService entradaService;
    @Inject
    private SaidaService saidaService;
    @Inject
    private ProdutoService produtoService;
    @Inject
    private AlmoxarifadoService almoxarifadoService;
    private List<AjusteEstoque> ajustesConsulta;
    private AjusteEstoque ajusteEstoque;
    private Entrada entrada;
    private Saida saida;
    private List<Produto> produtosEntrada;
    private List<Produto> produtosEntradaSelected;
    private List<Produto> produtosSaida;
    private List<Produto> produtosSaidaSelected;
    private List<EntradaProduto> entradasProdutoSelected;
    private List<SaidaProduto> saidasProdutoSelected;
    
    private String filtroProdutoNome;
    private String filtroCodigoBarras;
    private String filtroUsuarioResponsavel;
    private boolean filtroAjusteFinalizado = true;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        ajusteEstoque = new AjusteEstoque();
        saida = new Saida();
        entrada = new Entrada();
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        ajusteEstoque = ajusteEstoqueService.buscarPorIdFetchEntradaESaidaDeProdutos(ajusteEstoque.getId());
        saida = ajusteEstoque.getSaida();
        entrada = ajusteEstoque.getEntrada();
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }
    
    public String salvar() {
        try {
            if (ajusteEstoque.isNew()) {
                ajusteEstoque.setUsuarioCadastro(session.getUsuarioLogado());
                saida.setUsuarioCadastro(session.getUsuarioLogado());
                entrada.setUsuarioCadastro(session.getUsuarioLogado());
            }
            saida.setOrigem(OrigemTransacao.AJUSTEESTOQUE);
            saida.setAlmoxarifado(ajusteEstoque.getAlmoxarifadoSaida());
            entrada.setOrigem(OrigemTransacao.AJUSTEESTOQUE);
            entrada.setAlmoxarifado(ajusteEstoque.getAlmoxarifadoEntrada());
            if (ajusteEstoque.isFinalizado()) {
                saida.setStatus(StatusTransacao.FINALIZADO);
                entrada.setStatus(StatusTransacao.FINALIZADO);
            }
            saida = saidaService.salvar(saida);
            entrada = entradaService.salvar(entrada);
            ajusteEstoque.setSaida(saida);
            ajusteEstoque.setEntrada(entrada);
            ajusteEstoque = ajusteEstoqueService.salvar(ajusteEstoque);
            atualizaListas();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
            saidaService.verificarSeProdutosFicaramAbaixoDoEstoqueMinimoPorSaida(saida);
            return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
        } catch (ProdutoAbaixoEstoqueMinimoException ex) {
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getLocalizedMessage());
            RequestContext.getCurrentInstance().showMessageInDialog(mensagem);
            return null;
        } catch (HadoukenException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getLocalizedMessage()));
            return null;
        }
    }

    public String cancelarAlteracoes() {
        if (ajusteEstoque.isNew()) {
            ajusteEstoque = new AjusteEstoque();
            saida = new Saida();
            entrada = new Entrada();
        } else {
            ajusteEstoque = ajusteEstoqueService.buscarPorId(ajusteEstoque.getId());
            saida = ajusteEstoque.getSaida();
            entrada = ajusteEstoque.getEntrada();
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("produtoNome", getFiltroProdutoNome());
        filtros.put("codigoBarras", getFiltroCodigoBarras());
        filtros.put("usuarioResponsavel", getFiltroUsuarioResponsavel());
        filtros.put("ajusteFinalizado", isFiltroAjusteFinalizado());
        ajustesConsulta = ajusteEstoqueService.fetchEntradaESaidaDeProdutos(ajusteEstoqueService.buscarListagem(filtros));
        ajusteEstoque = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroProdutoNome(null);
        setFiltroCodigoBarras(null);
        setFiltroUsuarioResponsavel(null);
        setFiltroAjusteFinalizado(false);
        ajustesConsulta = null;
        ajusteEstoque = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<AjusteEstoque> getAjustes() {
        if (ajustesConsulta != null) {
            return ajustesConsulta;
        }
        return ajusteEstoqueService.fetchEntradaESaidaDeProdutos(ajusteEstoqueService.buscarTodos());
    }
    
    public List<Almoxarifado> getAlmoxarifados() {
        return almoxarifadoService.buscarTodosAtivos();
    } 

    public AjusteEstoque getAjusteEstoque() {
        if (ajusteEstoque == null) {
            ajusteEstoque = new AjusteEstoque();
        }
        return ajusteEstoque;
    }

    public void setAjusteEstoque(AjusteEstoque ajusteEstoque) {
        this.ajusteEstoque = ajusteEstoque;
    }

    public Entrada getEntrada() {
        if (entrada == null) {
            entrada = new Entrada();
        }
        return entrada;
    }

    public void setEntrada(Entrada entrada) {
        this.entrada = entrada;
    }

    public Saida getSaida() {
        if (saida == null) {
            saida = new Saida();
        }
        return saida;
    }

    public void setSaida(Saida saida) {
        this.saida = saida;
    }

    public List<Produto> getProdutosEntrada() {
        if (produtosEntrada == null) {
            produtosEntrada = produtoService.buscarAtivosArtesanal();
        }
        produtosEntrada.removeAll(entrada.getProdutos());
        return produtosEntrada;
    }

    public void setProdutosEntrada(List<Produto> produtosEntrada) {
        this.produtosEntrada = produtosEntrada;
    }

    public List<Produto> getProdutosEntradaSelected() {
        return produtosEntradaSelected;
    }

    public void setProdutosEntradaSelected(List<Produto> produtosEntradaSelected) {
        this.produtosEntradaSelected = produtosEntradaSelected;
    }

    public List<Produto> getProdutosSaida() {
        if (produtosSaida == null) {
            produtosSaida = produtoService.buscarAtivosMateriaPrima();
        }
        produtosSaida.removeAll(saida.getProdutos());
        return produtosSaida;
    }

    public void setProdutosSaida(List<Produto> produtosSaida) {
        this.produtosSaida = produtosSaida;
    }

    public List<Produto> getProdutosSaidaSelected() {
        return produtosSaidaSelected;
    }

    public void setProdutosSaidaSelected(List<Produto> produtosSaidaSelected) {
        this.produtosSaidaSelected = produtosSaidaSelected;
    }

    public List<EntradaProduto> getEntradasProdutoSelected() {
        return entradasProdutoSelected;
    }

    public void setEntradasProdutoSelected(List<EntradaProduto> entradasProdutoSelected) {
        this.entradasProdutoSelected = entradasProdutoSelected;
    }

    public List<SaidaProduto> getSaidasProdutoSelected() {
        return saidasProdutoSelected;
    }

    public void setSaidasProdutoSelected(List<SaidaProduto> saidasProdutoSelected) {
        this.saidasProdutoSelected = saidasProdutoSelected;
    }

    public void removerProdutosEntradaSelecionados() {
        for (EntradaProduto ep : getEntradasProdutoSelected()) {
            if (getEntrada().getEntradaProduto().contains(ep)) {
                getEntrada().getEntradaProduto().remove(ep);
            }
        }
        atualizaListas();
    }

    public void adicionarProdutosEntradaSelecionados() {
        List<EntradaProduto> ep = new ArrayList<>();
        for (Produto ps : getProdutosEntradaSelected()) {
            EntradaProduto entradaProduto = new EntradaProduto();
            entradaProduto.setProduto(ps);
            ep.add(entradaProduto);
        }
        getEntrada().getEntradaProduto().addAll(ep);
        atualizaListas();
    }

    public void removerProdutosSaidaSelecionados() {
        for (SaidaProduto ep : getSaidasProdutoSelected()) {
            if (getSaida().getSaidaProduto().contains(ep)) {
                getSaida().getSaidaProduto().remove(ep);
            }
        }
        atualizaListas();
    }

    public void adicionarProdutosSaidaSelecionados() {
        List<SaidaProduto> ep = new ArrayList<>();
        for (Produto ps : getProdutosSaidaSelected()) {
            SaidaProduto saidaProduto = new SaidaProduto();
            saidaProduto.setProduto(ps);
            ep.add(saidaProduto);
        }
        getSaida().getSaidaProduto().addAll(ep);
        atualizaListas();
    }
    
    public boolean isFinalizadoESalvo() {
        return getAjusteEstoque().isFinalizado() && !getAjusteEstoque().isNew();
    }

    public void atualizaListas() {
        produtosEntrada = null;
        produtosEntradaSelected = null;
        produtosSaida = null;
        produtosSaidaSelected = null;
        entradasProdutoSelected = null;
        saidasProdutoSelected = null;
    }

    public String getFiltroProdutoNome() {
        return filtroProdutoNome;
    }

    public void setFiltroProdutoNome(String filtroProdutoNome) {
        this.filtroProdutoNome = filtroProdutoNome;
    }

    public String getFiltroCodigoBarras() {
        return filtroCodigoBarras;
    }

    public void setFiltroCodigoBarras(String filtroCodigoBarras) {
        this.filtroCodigoBarras = filtroCodigoBarras;
    }

    public String getFiltroUsuarioResponsavel() {
        return filtroUsuarioResponsavel;
    }

    public void setFiltroUsuarioResponsavel(String filtroUsuarioResponsavel) {
        this.filtroUsuarioResponsavel = filtroUsuarioResponsavel;
    }

    public boolean isFiltroAjusteFinalizado() {
        return filtroAjusteFinalizado;
    }

    public void setFiltroAjusteFinalizado(boolean filtroAjusteFinalizado) {
        this.filtroAjusteFinalizado = filtroAjusteFinalizado;
    }
}
