package br.com.hadouken.view;

import br.com.hadouken.entity.GrupoProduto;
import br.com.hadouken.entity.Produto;
import br.com.hadouken.entity.SubtipoProduto;
import br.com.hadouken.entity.TipoProduto;
import br.com.hadouken.service.ProdutoService;
import br.com.hadouken.service.SubtipoProdutoService;
import br.com.hadouken.service.TipoProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.*;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class ProdutoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private ProdutoService produtoService;
    @Inject
    private TipoProdutoService tipoProdutoService;
    @Inject
    private SubtipoProdutoService subtipoProdutoService;
    private List<Produto> produtosConsulta;
    private Produto produto;
    private TipoProduto tipoProduto;

    private String filtroNome;
    private GrupoProduto[] filtroGrupo;
    private String filtroCodigoBarras;
    private boolean filtroEstoqueAbaixoDoMinimo;
    private boolean filtroAtivo = true;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        produto = new Produto();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (produto.isNew()) {
            produto.setUsuarioCadastro(session.getUsuarioLogado());
        }
        produto = produtoService.salvar(produto);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String cancelarAlteracoes() {
        if (produto.isNew()) {
            produto = new Produto();
        } else {
            produto = produtoService.buscarPorId(produto.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("nome", getFiltroNome());
        filtros.put("grupos", getFiltroGrupo());
        filtros.put("codigoBarras", getFiltroCodigoBarras());
        filtros.put("abaixoEstoqueMinimo", isFiltroEstoqueAbaixoDoMinimo());
        filtros.put("ativo", isFiltroAtivo());
        produtosConsulta = produtoService.buscarListagem(filtros);
        produto = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroNome(null);
        setFiltroGrupo(null);
        setFiltroCodigoBarras(null);
        setFiltroEstoqueAbaixoDoMinimo(false);
        setFiltroAtivo(true);
        produtosConsulta = null;
        produto = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<Produto> getProdutos() {
        if (produtosConsulta != null) {
            return produtosConsulta;
        }
        return produtoService.buscarTodos();
    }

    public Produto getProduto() {
        if (produto == null) {
            produto = new Produto();
        }
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public TipoProduto getTipoProduto() {
        if (tipoProduto == null) {
            if (getProduto().isNew()) {
                tipoProduto = new TipoProduto();
            } else {
                tipoProduto = getProduto().getSubtipoProduto().getTipoProduto();
            }
        }
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public String getFiltroNome() {
        return filtroNome;
    }

    public void setFiltroNome(String filtroNome) {
        this.filtroNome = filtroNome;
    }

    public GrupoProduto[] getGrupos() {
        return GrupoProduto.values();
    }

    public List<TipoProduto> getTiposAtivos() {
        List<TipoProduto> tipos = tipoProdutoService.buscarTodosAtivos();
        if (!tipos.isEmpty()) {
            tipoProduto = tipos.get(0);
        }
        return tipos;
    }

    public List<SubtipoProduto> getSubtiposPorTipo() {
        if (!getTipoProduto().isNew()) {
            return subtipoProdutoService.buscarTodosAtivosPorTipo(getTipoProduto());
        }
        return new ArrayList<>();
    }

    public boolean isMateriaPrima() {
        if (getProduto().getGrupo() != null) {
            return getProduto().getGrupo().equals(GrupoProduto.MATERIAPRIMA);
        }
        return false;
    }

    public GrupoProduto[] getFiltroGrupo() {
        if (filtroGrupo == null) {
            filtroGrupo = getGrupos();
        }
        return filtroGrupo;
    }

    public void setFiltroGrupo(GrupoProduto[] filtroGrupo) {
        this.filtroGrupo = filtroGrupo;
    }
    
    public String getFiltroCodigoBarras() {
        return filtroCodigoBarras;
    }

    public void setFiltroCodigoBarras(String filtroCodigoBarras) {
        this.filtroCodigoBarras = filtroCodigoBarras;
    }

    public boolean isFiltroEstoqueAbaixoDoMinimo() {
        return filtroEstoqueAbaixoDoMinimo;
    }

    public void setFiltroEstoqueAbaixoDoMinimo(boolean filtroEstoqueAbaixoDoMinimo) {
        this.filtroEstoqueAbaixoDoMinimo = filtroEstoqueAbaixoDoMinimo;
    }

    public boolean isFiltroAtivo() {
        return filtroAtivo;
    }

    public void setFiltroAtivo(boolean filtroAtivo) {
        this.filtroAtivo = filtroAtivo;
    }
        
}
