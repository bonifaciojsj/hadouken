package br.com.hadouken.view;

import br.com.hadouken.entity.TipoProduto;
import br.com.hadouken.service.TipoProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author u0181089
 */
@Named
@SessionScoped
public class TipoProdutoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private TipoProdutoService tipoProdutoService;
    private TipoProduto tipoProduto;

    public List<TipoProduto> getTiposProduto() {
        return tipoProdutoService.buscarTodos();
    }

    public String voltar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        tipoProduto = new TipoProduto();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (tipoProduto.isNew()) {
            tipoProduto.setUsuarioCadastro(session.getUsuarioLogado());
        }
        tipoProduto = tipoProdutoService.salvar(tipoProduto);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;

    }

    public String cancelarAlteracoes() {
        if (tipoProduto.isNew()) {
            tipoProduto = new TipoProduto();
        } else {
            tipoProduto = tipoProdutoService.buscarPorId(tipoProduto.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public TipoProduto getTipoProduto() {
        if (tipoProduto == null) {
            tipoProduto = new TipoProduto();
        }
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

}
