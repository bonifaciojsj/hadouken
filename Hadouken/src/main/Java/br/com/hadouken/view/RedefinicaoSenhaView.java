package br.com.hadouken.view;

import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.service.UsuarioService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.util.PasswordUtil;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class RedefinicaoSenhaView implements Serializable {
    
    @Inject
    private SessionController sessao;
    @Inject
    private UsuarioService usuarioService;
    private String senhaAtual;
    private String novaSenha;
    private String novaSenhaConfirmacao;
    
    public String redefinirSenha() {
        try {
            sessao.setUsuarioLogado(usuarioService.salvarUsuarioComNovaSenha(sessao.getUsuarioLogado(), senhaAtual, novaSenha, novaSenhaConfirmacao));
            return "/webpage/home.xhtml?faces-redirect=true";
        } catch (HadoukenException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", ex.getMessage()));
        }
        return "/webpage/login/redefinirSenha.xhtml";        
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = PasswordUtil.getEncryptedPassword(senhaAtual);
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = PasswordUtil.getEncryptedPassword(novaSenha);
    }

    public String getNovaSenhaConfirmacao() {
        return novaSenhaConfirmacao;
    }

    public void setNovaSenhaConfirmacao(String novaSenhaConfirmacao) {
        this.novaSenhaConfirmacao = PasswordUtil.getEncryptedPassword(novaSenhaConfirmacao);
    }
    
}
