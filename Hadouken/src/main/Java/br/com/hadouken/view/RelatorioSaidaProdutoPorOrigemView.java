package br.com.hadouken.view;

import br.com.hadouken.service.SaidaService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.util.FileUtil;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class RelatorioSaidaProdutoPorOrigemView implements Serializable {
    
    private static final String REPORT_FILENAME = "relatorioSaidaProdutosPorOrigem";
    
    @Inject
    private SessionController session;
    @Inject
    private SaidaService saidaService;
    private boolean relatorioGerado;
    
    public String gerarRelatorio() {
        FileUtil.gerarRelatorio(saidaService.buscarTodasGroupByOrigem(), session.getUsuarioLogado(), REPORT_FILENAME);
        relatorioGerado = true;
        return FacesNavigationUtil.REDIRECT_OUTCOME_REPORT;
    }    
    
    public String downloadUrl() {
       return "/download?usuario=" + session.getUsuarioLogado().getId()
               + "&fileName=" + REPORT_FILENAME + ".pdf"
               + "&fileType=application/pdf";
    }
    
    public boolean getRelatorioGerado() {
        return relatorioGerado;
    }
    
}
