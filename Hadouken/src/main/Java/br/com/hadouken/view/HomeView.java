package br.com.hadouken.view;

import br.com.hadouken.entity.*;
import br.com.hadouken.service.ProdutoService;
import br.com.hadouken.service.SaidaService;
import br.com.hadouken.service.SubtipoProdutoService;
import br.com.hadouken.service.TipoProdutoService;
import java.io.Serializable;
import java.util.*;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.*;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class HomeView implements Serializable {

    @Inject
    private ProdutoService produtoService;
    @Inject
    private TipoProdutoService tipoProdutoService;
    @Inject
    private SubtipoProdutoService subtipoProdutoService;
    @Inject
    private SaidaService saidaService;
    private BarChartModel graficoSaidaProdutosPorSubtipo;
    private PieChartModel graficoProdutosPorGrupo;
    private PieChartModel graficoProdutosPorTipo;
    private MeterGaugeChartModel graficoProdutosAbaixoDoEstoqueMinimo;

    public BarChartModel getGraficoSaidaProdutosPorSubtipo() {
        graficoSaidaProdutosPorSubtipo = new BarChartModel();
        for (SubtipoProduto subtipo : subtipoProdutoService.buscarTodosAtivos()) {
            ChartSeries subtipoSerie = new ChartSeries();
            int quantidade = 0;
            for (Saida saida : saidaService.buscarTodasFinalizadasFetchSaidaProduto()) {
                for (SaidaProduto saidaProduto : saida.getSaidaProduto()) {
                    if (saidaProduto.getProduto().getSubtipoProduto() != null
                            && saidaProduto.getProduto().getSubtipoProduto().equals(subtipo)) {
                        subtipoSerie.setLabel(saidaProduto.getProduto().getSubtipoProduto().getNome());
                        quantidade += saidaProduto.getQuantidade();
                    }
                }
            }
            if (quantidade > 0) {
                subtipoSerie.set("Quantidade", quantidade);
                graficoSaidaProdutosPorSubtipo.addSeries(subtipoSerie);
            }
        }
        Collections.sort(graficoSaidaProdutosPorSubtipo.getSeries(), new Comparator<ChartSeries>() {
            @Override
            public int compare(ChartSeries c1, ChartSeries c2) {
                return Integer.valueOf(c2.getData().get("Quantidade").intValue()).compareTo(c1.getData().get("Quantidade").intValue());
            }
        });
        graficoSaidaProdutosPorSubtipo.setAnimate(true);
        graficoSaidaProdutosPorSubtipo.setTitle("Saída de produtos por subtipo");
        graficoSaidaProdutosPorSubtipo.setLegendPosition("w");
        return graficoSaidaProdutosPorSubtipo;
    }

    public PieChartModel getGraficoProdutosPorGrupo() {
        graficoProdutosPorGrupo = new PieChartModel();

        int produtosIndustrializados = 0;
        int produtosArtesanais = 0;
        int produtosMateriaPrima = 0;
        for (Produto produto : produtoService.buscarAtivos()) {
            switch (produto.getGrupo()) {
                case INDUSTRIALIZADO:
                    produtosIndustrializados++;
                    break;
                case ARTESANAL:
                    produtosArtesanais++;
                    break;
                case MATERIAPRIMA:
                    produtosMateriaPrima++;
                    break;
            }
        }
        graficoProdutosPorGrupo.set(GrupoProduto.INDUSTRIALIZADO.getDescricao(), produtosIndustrializados);
        graficoProdutosPorGrupo.set(GrupoProduto.ARTESANAL.getDescricao(), produtosArtesanais);
        graficoProdutosPorGrupo.set(GrupoProduto.MATERIAPRIMA.getDescricao(), produtosMateriaPrima);

        graficoProdutosPorGrupo.setTitle("Produtos por grupo");
        graficoProdutosPorGrupo.setLegendPosition("w");
        return graficoProdutosPorGrupo;
    }

    public PieChartModel getGraficoProdutosPorTipo() {
        graficoProdutosPorTipo = new PieChartModel();
        for (TipoProduto tipo : tipoProdutoService.buscarTodosAtivos()) {
            int quantidade = 0;
            for (Produto produto : produtoService.buscarAtivos()) {
                if (produto.getSubtipoProduto() != null && produto.getSubtipoProduto().getTipoProduto().equals(tipo)) {
                    quantidade++;
                }
            }
            graficoProdutosPorTipo.set(tipo.getNome(), quantidade);
        }

        graficoProdutosPorTipo.setTitle("Produtos por tipo");
        graficoProdutosPorTipo.setLegendPosition("w");
        return graficoProdutosPorTipo;
    }

    public MeterGaugeChartModel getGraficoProdutosAbaixoDoEstoqueMinimo() {
        final List<Produto> produtosAtivos = produtoService.buscarAtivos();
        final List<Produto> produtosAbaixoDoEstoqueMinimo = produtoService.buscarAtivosComEstoqueAbaixoDoMinimo();
        List<Number> intervals = new ArrayList<Number>() {
            {
                add(produtosAtivos.size() / 5);
                add(produtosAtivos.size() / 3);
                add(produtosAtivos.size() / 2);
                add(produtosAtivos.size());
            }
        };
        graficoProdutosAbaixoDoEstoqueMinimo = new MeterGaugeChartModel(produtosAbaixoDoEstoqueMinimo.size(), intervals);
        graficoProdutosAbaixoDoEstoqueMinimo.setTitle("Produtos abaixo do estoque mínimo");
        graficoProdutosAbaixoDoEstoqueMinimo.setGaugeLabel("Quantidade");
        graficoProdutosAbaixoDoEstoqueMinimo.setSeriesColors("66cc66,93b75f,E7E658,cc6666");

        return graficoProdutosAbaixoDoEstoqueMinimo;
    }

}
