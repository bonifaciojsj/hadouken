package br.com.hadouken.view;

import br.com.hadouken.entity.SubtipoProduto;
import br.com.hadouken.entity.TipoProduto;
import br.com.hadouken.service.SubtipoProdutoService;
import br.com.hadouken.service.TipoProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author u0181089
 */
@Named
@SessionScoped
public class SubtipoProdutoView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private SubtipoProdutoService subtipoProdutoService;
    @Inject
    private TipoProdutoService tipoProdutoService;
    private SubtipoProduto subtipoProduto;

    public List<SubtipoProduto> getSubtiposProduto() {
        return subtipoProdutoService.buscarTodos();
    }

    public String voltar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        subtipoProduto = new SubtipoProduto();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (subtipoProduto.isNew()) {
            subtipoProduto.setUsuarioCadastro(session.getUsuarioLogado());
        }
        subtipoProduto = subtipoProdutoService.salvar(subtipoProduto);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;

    }

    public String cancelarAlteracoes() {
        if (subtipoProduto.isNew()) {
            subtipoProduto = new SubtipoProduto();
        } else {
            subtipoProduto = subtipoProdutoService.buscarPorId(subtipoProduto.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public SubtipoProduto getSubtipoProduto() {
        if (subtipoProduto == null) {
            subtipoProduto = new SubtipoProduto();
        }
        return subtipoProduto;
    }

    public void setSubtipoProduto(SubtipoProduto tipoProduto) {
        this.subtipoProduto = tipoProduto;
    }
    
    public List<TipoProduto> getTiposAtivos() {
        return tipoProdutoService.buscarTodosAtivos();
    }

}
