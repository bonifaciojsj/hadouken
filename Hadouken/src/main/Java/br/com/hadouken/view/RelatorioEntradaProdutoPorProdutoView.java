package br.com.hadouken.view;

import br.com.hadouken.service.EntradaService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.util.FileUtil;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class RelatorioEntradaProdutoPorProdutoView implements Serializable {
    
    private static final String REPORT_FILENAME = "relatorioEntradaProdutosPorProduto";
    
    @Inject
    private SessionController session;
    @Inject
    private EntradaService entradaService;
    private boolean relatorioGerado;
    
    public String gerarRelatorio() {
        FileUtil.gerarRelatorio(entradaService.buscarEntradaProdutoGroupByProduto(), session.getUsuarioLogado(), REPORT_FILENAME);
        relatorioGerado = true;
        return FacesNavigationUtil.REDIRECT_OUTCOME_REPORT;
    }    
    
    public String downloadUrl() {
       return "/download?usuario=" + session.getUsuarioLogado().getId()
               + "&fileName=" + REPORT_FILENAME + ".pdf"
               + "&fileType=application/pdf";
    }
    
    public boolean getRelatorioGerado() {
        return relatorioGerado;
    }
    
}
