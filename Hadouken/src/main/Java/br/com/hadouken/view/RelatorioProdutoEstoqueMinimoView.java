package br.com.hadouken.view;

import br.com.hadouken.service.ClienteService;
import br.com.hadouken.service.ProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.util.FileUtil;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class RelatorioProdutoEstoqueMinimoView implements Serializable {
    
    private static final String REPORT_FILENAME = "relatorioProdutoEstoqueMinimo";
    
    @Inject
    private SessionController session;
    @Inject
    private ProdutoService produtoService;
    private boolean relatorioGerado;
    
    public String gerarRelatorio() {
        FileUtil.gerarRelatorio(produtoService.buscarAtivosComEstoqueIgualOuAbaixoDoMinimo(),
                session.getUsuarioLogado(), REPORT_FILENAME);
        relatorioGerado = true;
        return FacesNavigationUtil.REDIRECT_OUTCOME_REPORT;
    }
    
    public String downloadUrl() {
       return "/download?usuario=" + session.getUsuarioLogado().getId()
               + "&fileName=" + REPORT_FILENAME + ".pdf"
               + "&fileType=application/pdf";
    }

    public boolean getRelatorioGerado() {
        return relatorioGerado;
    }
    
}
