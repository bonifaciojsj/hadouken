package br.com.hadouken.view;

import br.com.hadouken.entity.Estado;
import br.com.hadouken.entity.Fornecedor;
import br.com.hadouken.service.FornecedorService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class FornecedorView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private FornecedorService fornecedorService;
    private List<Fornecedor> fornecedoresConsulta;
    private Fornecedor fornecedor;

    private String filtroRazaoSocial;    
    private String filtroCnpj;
    private String filtroEmail;
    private String filtroTelefone;
    private boolean filtroAtivo = true;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        fornecedor = new Fornecedor();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        if (fornecedor.isNew()) {
            fornecedor.setUsuarioCadastro(session.getUsuarioLogado());
        }
        fornecedor = fornecedorService.salvar(fornecedor);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String cancelarAlteracoes() {
        if (fornecedor.isNew()) {
            fornecedor = new Fornecedor();
        } else {
            fornecedor = fornecedorService.buscarPorId(fornecedor.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("razaoSocial", getFiltroRazaoSocial());
        filtros.put("cnpj", getFiltroCnpj());
        filtros.put("email", getFiltroEmail());
        filtros.put("telefone", getFiltroTelefone());
        filtros.put("ativo", isFiltroAtivo());
        fornecedoresConsulta = fornecedorService.buscarListagem(filtros);
        fornecedor = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroRazaoSocial(null);
        setFiltroAtivo(true);
        setFiltroCnpj(null);
        setFiltroEmail(null);
        setFiltroTelefone(null);
        fornecedoresConsulta = null;
        fornecedor = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<Fornecedor> getFornecedores() {
        if (fornecedoresConsulta != null) {
            return fornecedoresConsulta;
        }
        return fornecedorService.buscarTodos();
    }

    public Fornecedor getFornecedor() {
        if (fornecedor == null) {
            fornecedor = new Fornecedor();
        }
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getFiltroRazaoSocial() {
        return filtroRazaoSocial;
    }

    public void setFiltroRazaoSocial(String filtroRazaoSocial) {
        this.filtroRazaoSocial = filtroRazaoSocial;
    }

    public Estado[] getEstados() {
        return Estado.values();
    }

    public String getFiltroCnpj() {
        return filtroCnpj;
    }

    public void setFiltroCnpj(String filtroCnpj) {
        this.filtroCnpj = filtroCnpj;
    }

    public String getFiltroEmail() {
        return filtroEmail;
    }

    public void setFiltroEmail(String filtroEmail) {
        this.filtroEmail = filtroEmail;
    }

    public String getFiltroTelefone() {
        return filtroTelefone;
    }

    public void setFiltroTelefone(String filtroTelefone) {
        this.filtroTelefone = filtroTelefone;
    }

    public boolean isFiltroAtivo() {
        return filtroAtivo;
    }

    public void setFiltroAtivo(boolean filtroAtivo) {
        this.filtroAtivo = filtroAtivo;
    }    

}
