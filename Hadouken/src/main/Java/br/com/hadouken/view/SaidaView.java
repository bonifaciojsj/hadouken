package br.com.hadouken.view;

import br.com.hadouken.entity.*;
import br.com.hadouken.exception.HadoukenException;
import br.com.hadouken.exception.ProdutoAbaixoEstoqueMinimoException;
import br.com.hadouken.service.AlmoxarifadoService;
import br.com.hadouken.service.SaidaService;
import br.com.hadouken.service.ClienteService;
import br.com.hadouken.service.ProdutoService;
import br.com.hadouken.session.SessionController;
import br.com.hadouken.view.util.FacesNavigationUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jose.bonifacio
 */
@Named
@SessionScoped
public class SaidaView implements Serializable {

    @Inject
    private SessionController session;
    @Inject
    private SaidaService saidaService;
    @Inject
    private ClienteService clienteService;
    @Inject
    private ProdutoService produtoService;
    @Inject
    private AlmoxarifadoService almoxarifadoService;
    private List<Saida> saidasConsulta;
    private Saida saida;
    private List<Cliente> clientes;
    private List<Produto> produtos;
    private List<Produto> produtosSelected;
    private List<SaidaProduto> saidaProdutoSelected;

    private String filtroNomeCliente;
    private String filtroProdutoNome;
    private String filtroCodigoBarras;
    private StatusTransacao[] filtroStatusTransacao;

    public String voltar() {
        novaBusca();
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novo() {
        saida = new Saida();
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String editar() {
        saida = saidaService.buscarPorIdFetchSaidaProduto(saida.getId());
        atualizaListas();
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String salvar() {
        try {
            if (saida.isNew()) {
                saida.setUsuarioCadastro(session.getUsuarioLogado());
            }
            saida = saidaService.salvar(saida);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", "Registro salvo!"));
            saidaService.verificarSeProdutosFicaramAbaixoDoEstoqueMinimoPorSaida(saida);
            atualizaListas();
            return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
        } catch (ProdutoAbaixoEstoqueMinimoException ex) {
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getLocalizedMessage());
            RequestContext.getCurrentInstance().showMessageInDialog(mensagem);
            return null;            
        } catch (HadoukenException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro:", ex.getLocalizedMessage()));
            return null;
        }
    }

    public String cancelarAlteracoes() {
        if (saida.isNew()) {
            saida = new Saida();
        } else {
            saida = saidaService.buscarPorIdFetchSaidaProduto(saida.getId());
        }
        return FacesNavigationUtil.REDIRECT_OUTCOME_INCLUIR_EDITAR;
    }

    public String buscar() {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("filtroNomeCliente", getFiltroNomeCliente());
        filtros.put("produtoNome", getFiltroProdutoNome());
        filtros.put("codigoBarras", getFiltroCodigoBarras());
        filtros.put("status", getFiltroStatusTransacao());
        saidasConsulta = saidaService.buscarListagem(filtros);
        saida = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public String novaBusca() {
        setFiltroRazaoSocialCliente(null);
        setFiltroProdutoNome(null);
        setFiltroCodigoBarras(null);
        setFiltroStatusTransacao(null);
        saidasConsulta = null;
        saida = null;
        return FacesNavigationUtil.REDIRECT_OUTCOME_LISTAR;
    }

    public List<Saida> getSaidas() {
        if (saidasConsulta != null) {
            return saidasConsulta;
        }
        return saidaService.buscarTodas();
    }

    public Saida getSaida() {
        if (saida == null) {
            saida = new Saida();
        }
        return saida;
    }

    public void setSaida(Saida saida) {
        this.saida = saida;
    }

    public List<Cliente> getClientes() {
        if (clientes == null) {
            clientes = clienteService.buscarTodos();
        }
        return clientes;
    }
    
    public List<Almoxarifado> getAlmoxarifados() {
        return almoxarifadoService.buscarTodosAtivos();
    } 

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Produto> getProdutos() {
        if (produtos == null) {
            produtos = produtoService.buscarAtivosComercializaveis();
        }
        produtos.removeAll(getSaida().getProdutos());
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public List<Produto> getProdutosSelected() {
        return produtosSelected;
    }

    public void setProdutosSelected(List<Produto> produtosSelected) {
        this.produtosSelected = produtosSelected;
    }

    public List<SaidaProduto> getSaidaProdutoSelected() {
        return saidaProdutoSelected;
    }

    public void setSaidaProdutoSelected(List<SaidaProduto> saidaProdutoSelected) {
        this.saidaProdutoSelected = saidaProdutoSelected;
    }

    public String getFiltroNomeCliente() {
        return filtroNomeCliente;
    }
    
    public void setFiltroNomeCliente(String filtroNomeCliente) {
        this.filtroNomeCliente = filtroNomeCliente;
    }
    
    public void setFiltroRazaoSocialCliente(String filtroNomeCliente) {
        this.filtroNomeCliente = filtroNomeCliente;
    }

    public StatusTransacao[] getStatusTrancacao() {
        return StatusTransacao.values();
    }

    public boolean saidaFinalizada() {
        return saida.getStatus().equals(StatusTransacao.FINALIZADO);
    }
    
    public boolean saidaFinalizadaESalva() {
        return saidaFinalizada() && !getSaida().isNew();
    }

    public String getFiltroProdutoNome() {
        return filtroProdutoNome;
    }

    public void setFiltroProdutoNome(String filtroProdutoNome) {
        this.filtroProdutoNome = filtroProdutoNome;
    }

    public String getFiltroCodigoBarras() {
        return filtroCodigoBarras;
    }

    public void setFiltroCodigoBarras(String filtroCodigoBarras) {
        this.filtroCodigoBarras = filtroCodigoBarras;
    }

    public StatusTransacao[] getFiltroStatusTransacao() {
        if (filtroStatusTransacao == null) {
            filtroStatusTransacao = getStatusTrancacao();
        }
        return filtroStatusTransacao;
    }

    public void setFiltroStatusTransacao(StatusTransacao[] filtroStatusTransacao) {
        this.filtroStatusTransacao = filtroStatusTransacao;
    }
    
    public void removerProdutosSelecionados() {
        for (SaidaProduto ep : getSaidaProdutoSelected()) {
            if (getSaida().getSaidaProduto().contains(ep)) {
                getSaida().getSaidaProduto().remove(ep);
            }
        }
        atualizaListas();
    }

    public void adicionarProdutosSelecionados() {
        List<SaidaProduto> ep = new ArrayList<>();
        for (Produto ps : getProdutosSelected()) {
            SaidaProduto saidaProduto = new SaidaProduto();
            saidaProduto.setProduto(ps);
            ep.add(saidaProduto);
        }
        getSaida().getSaidaProduto().addAll(ep);
        atualizaListas();
    }

    public void atualizaListas() {
        this.saidaProdutoSelected = null;
        this.produtos = null;
        this.produtosSelected = null;
        this.clientes = null;
    }
}
