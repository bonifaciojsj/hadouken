package br.com.hadouken.entity;

/**
 *
 * @author jose.bonifacio
 */
public enum TipoPessoa {
    
    FISICA("Pessoa física", 0),
    JURIDICA("Pessoa jurídica", 1);
    
    private final String descricao;
    private final int index;

    private TipoPessoa(String descricao, int index) {
        this.descricao = descricao;
        this.index = index;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getIndex() {
        return index;
    }
}
