package br.com.hadouken.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "entrada_produto")
public class EntradaProduto implements Serializable, PersistentEntity<EntradaProdutoPK> {
    
    @EmbeddedId
    private EntradaProdutoPK id;
    @ManyToOne
    @JoinColumn(name = "entrada_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Entrada entrada;
    @ManyToOne
    @JoinColumn(name = "produto_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Produto produto;
    private Integer quantidade;

    public EntradaProduto() {
        quantidade = 1;
    }

    @Override
    public EntradaProdutoPK getId() {
        return id;
    }

    @Override
    public void setId(EntradaProdutoPK id) {
        this.id = id;
    }

    public Entrada getEntrada() {
        return entrada;
    }

    public void setEntrada(Entrada entrada) {
        this.entrada = entrada;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
    
    @Override
    public boolean isNew() {
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EntradaProduto other = (EntradaProduto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
