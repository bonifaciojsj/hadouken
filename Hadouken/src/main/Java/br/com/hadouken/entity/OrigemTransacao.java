package br.com.hadouken.entity;

/**
 *
 * @author u0181089
 */
public enum OrigemTransacao {
    
    NORMAL("Normal", 0),
    AJUSTEESTOQUE("Ajuste de estoque", 1);
    
    private final String descricao;
    private final int index;

    private OrigemTransacao(String descricao, int index) {
        this.descricao = descricao;
        this.index = index;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getIndex() {
        return index;
    }
    
}
