package br.com.hadouken.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "saida_produto")
public class SaidaProduto implements Serializable, PersistentEntity<SaidaProdutoPK> {
    
    @EmbeddedId
    private SaidaProdutoPK id;
    @ManyToOne
    @JoinColumn(name = "saida_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Saida saida;
    @ManyToOne
    @JoinColumn(name = "produto_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Produto produto;
    private Integer quantidade;

    public SaidaProduto() {
        quantidade = 1;
    }

    @Override
    public SaidaProdutoPK getId() {
        return id;
    }

    @Override
    public void setId(SaidaProdutoPK id) {
        this.id = id;
    }

    public Saida getSaida() {
        return saida;
    }

    public void setSaida(Saida saida) {
        this.saida = saida;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
    
    @Override
    public boolean isNew() {
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SaidaProduto other = (SaidaProduto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
