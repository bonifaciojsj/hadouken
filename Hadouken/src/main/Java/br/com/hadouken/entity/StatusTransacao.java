package br.com.hadouken.entity;

/**
 *
 * @author jose.bonifacio
 */
public enum StatusTransacao {
    
    EM_ABERTO("Em aberto", 0),
    FINALIZADO("Finalizado", 1),
    CANCELADO("Cancelado", 2);
    
    private final String descricao;
    private final int index;

    private StatusTransacao(String status, int index) {
        descricao = status;
        this.index = index;
    }

    public String getDescricao() {
        return descricao;
    }
    
    public int getIndex() {
        return this.index;
    }
}
