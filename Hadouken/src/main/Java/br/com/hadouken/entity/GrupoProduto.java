package br.com.hadouken.entity;

/**
 *
 * @author u0181089
 */
public enum GrupoProduto {
    INDUSTRIALIZADO("Industrializado", 0),
    ARTESANAL("Artesanal", 1),
    MATERIAPRIMA("Matéria-prima", 2);

    private final String descricao;
    private final int index;

    private GrupoProduto(String descricao, int index) {
        this.descricao = descricao;
        this.index = index;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getIndex() {
        return this.index;
    }
}
