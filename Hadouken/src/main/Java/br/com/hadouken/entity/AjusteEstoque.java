package br.com.hadouken.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author u0181089
 */
@Entity
@Table(name = "ajusteestoque")
public class AjusteEstoque implements PersistentEntity<Long>, Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @NotNull
    private Saida saida;
    @ManyToOne
    @NotNull
    private Entrada entrada;
    private boolean finalizado;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCadastro;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataConclusao;
    @ManyToOne
    @NotNull
    private Almoxarifado almoxarifadoSaida;
    @ManyToOne
    @NotNull
    private Almoxarifado almoxarifadoEntrada;
    @ManyToOne
    @NotNull
    private Usuario usuarioCadastro;

    public AjusteEstoque() {
        dataCadastro = new Date();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Saida getSaida() {
        return saida;
    }

    public void setSaida(Saida saida) {
        this.saida = saida;
    }

    public Entrada getEntrada() {
        return entrada;
    }

    public void setEntrada(Entrada entrada) {
        this.entrada = entrada;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public Almoxarifado getAlmoxarifadoSaida() {
        return almoxarifadoSaida;
    }

    public void setAlmoxarifadoSaida(Almoxarifado almoxarifadoSaida) {
        this.almoxarifadoSaida = almoxarifadoSaida;
    }

    public Almoxarifado getAlmoxarifadoEntrada() {
        return almoxarifadoEntrada;
    }

    public void setAlmoxarifadoEntrada(Almoxarifado almoxarifadoEntrada) {
        this.almoxarifadoEntrada = almoxarifadoEntrada;
    }
    
    public Usuario getUsuarioCadastro() {
        return usuarioCadastro;
    }

    public void setUsuarioCadastro(Usuario usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AjusteEstoque other = (AjusteEstoque) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
