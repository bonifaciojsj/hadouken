package br.com.hadouken.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jose.bonifacio
 */
@Entity
@Table(name = "saida")
public class Saida implements PersistentEntity<Long>, Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Cliente cliente;
    private String observacao;
    private StatusTransacao status;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCadastro;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataConclusao;
    @OneToMany(mappedBy = "saida",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<SaidaProduto> saidaProduto;
    @NotNull
    private OrigemTransacao origem;
    @NotNull
    private boolean ativo;
    @ManyToOne
    @NotNull
    private Almoxarifado almoxarifado;
    @ManyToOne
    @NotNull
    private Usuario usuarioCadastro;

    public Saida() {
        status = StatusTransacao.EM_ABERTO;
        ativo = true;
        dataCadastro = new Date();
        saidaProduto = new ArrayList<>();
        origem = OrigemTransacao.NORMAL;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public StatusTransacao getStatus() {
        return status;
    }

    public void setStatus(StatusTransacao status) {
        this.status = status;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public List<SaidaProduto> getSaidaProduto() {
        return saidaProduto;
    }

    public void setSaidaProduto(List<SaidaProduto> saidaProduto) {
        this.saidaProduto = saidaProduto;
    }
    
    public List<Produto> getProdutos() {
        List<Produto> produtos = new ArrayList<>();
        for (SaidaProduto ep : getSaidaProduto()) {
            produtos.add(ep.getProduto());
        }
        return produtos;
    }
    
    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Almoxarifado getAlmoxarifado() {
        return almoxarifado;
    }

    public void setAlmoxarifado(Almoxarifado almoxarifado) {
        this.almoxarifado = almoxarifado;
    }   
    
    public Usuario getUsuarioCadastro() {
        return usuarioCadastro;
    }

    public void setUsuarioCadastro(Usuario usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }

    public OrigemTransacao getOrigem() {
        return origem;
    }

    public void setOrigem(OrigemTransacao origem) {
        this.origem = origem;
    }
    
    @Override
    public boolean isNew() {
        return this.id == null;
    } 

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Saida other = (Saida) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
