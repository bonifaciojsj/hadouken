package br.com.hadouken.exception;

/**
 *
 * @author jose.bonifacio
 */
public class ProdutoAbaixoEstoqueMinimoException extends HadoukenException {

    public ProdutoAbaixoEstoqueMinimoException(String message) {
        super(message);
    }

    public ProdutoAbaixoEstoqueMinimoException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
