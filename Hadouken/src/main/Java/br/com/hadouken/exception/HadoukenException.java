package br.com.hadouken.exception;

/**
 *
 * @author jose.bonifacio
 */
public class HadoukenException extends Exception {

    public HadoukenException(String message) {
        super(message);
    }

    public HadoukenException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
