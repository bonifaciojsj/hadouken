package br.com.hadouken.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jose.bonifacio
 */
@WebServlet(name = "FileDownloadServlet", urlPatterns = "/download")
public class FileDownloadServlet extends HttpServlet{
    
    private final static String FILES_PATH = "C:/Hadouken/Files/";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         String usuarioId = request.getParameter("usuario");
         String fileName = request.getParameter("fileName");
         String fileType = request.getParameter("fileType");
         String fileTotalPath = FILES_PATH.concat(usuarioId.concat("/".concat(fileName)));

         response.setContentType(fileType);

         response.setHeader("Content-disposition","attachment; " + fileName);


         File file = new File(fileTotalPath);

         OutputStream out = response.getOutputStream();
         FileInputStream in = new FileInputStream(file);
         byte[] buffer = new byte[4096];
         int length;
         while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
         }
         in.close();
         out.flush();
    }
}
