package br.com.hadouken.control;

import br.com.hadouken.entity.Produto;
import br.com.hadouken.exception.ProdutoAbaixoEstoqueMinimoException;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author u0181089
 */
@Stateless
public class ProdutoControl implements Serializable {

    public void verificarSeEstoqueFicouAbaixoDoMinimo(Produto produto) throws ProdutoAbaixoEstoqueMinimoException {
        if (produto.isAbaixoEstoqueMinimo()) {
            throw new ProdutoAbaixoEstoqueMinimoException("O produto " + produto.getNome() + " está com estoque abaixo de seu mínimo.");
        }
    }

    public void verificarSeEstoquesFicaramAbaixoDoMinimo(List<Produto> produtos) throws ProdutoAbaixoEstoqueMinimoException {
        String produtosComEstoqueAbaixoDoMinimo = "";
        for (Produto produto : produtos) {
            if (produto.isAbaixoEstoqueMinimo()) {
                if (!produtosComEstoqueAbaixoDoMinimo.isEmpty()) {
                    produtosComEstoqueAbaixoDoMinimo += ", ";
                }
                produtosComEstoqueAbaixoDoMinimo += produto.getNome();
            }
        }
        if (!produtosComEstoqueAbaixoDoMinimo.isEmpty()) {
            throw new ProdutoAbaixoEstoqueMinimoException("O(s) produto(s) " + produtosComEstoqueAbaixoDoMinimo + " está(ão) com o(s) estoque(s) abaixo de seu(s) mínimo(s).");

        }
    }
}
