package br.com.hadouken.dao;

import br.com.hadouken.entity.PersistentEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.HibernateProxyHelper;

/**
 *
 * @author jose.bonifacio
 * @param <E>
 * @param <PK>
 */
public class DAOImpl<E, PK extends Serializable> implements DAO<E, PK> {

    private final EntityManager em;
    private final Class<E> entityClass;

    public DAOImpl(EntityManager em, Class<E> entityClass) {
        this.em = em;
        this.entityClass = entityClass;
    }

    @Override
    public <E extends PersistentEntity> E save(E entity) {
        boolean isNew = entity.isNew();
        try {
            if (isNew) {
                em.persist(entity);
                em.flush();
            } else {
                Class type = HibernateProxyHelper.getClassWithoutInitializingProxy(unproxyEntity(entity));
                em.getReference(type, entity.getId());
                entity = em.merge(entity);
            }
        } catch (PersistenceException ex) {
            if (isNew) {
                entity.setId(null);
            }
            throw ex;
        }
        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E extends PersistentEntity> void remove(E entity) {
        if (em.contains(entity)) {
            em.remove(entity);
        } else if (!entity.isNew()) {
            Class type = HibernateProxyHelper.getClassWithoutInitializingProxy(unproxyEntity(entity));
            Object reference = em.getReference(type, entity.getId());
            em.remove(reference);
        }
        em.flush();
    }

    @Override
    @SuppressWarnings("unchecked")
    public E findByPrimaryKey(PK pk) {
        E entity = em.find(entityClass, pk);
        if (entity == null) {
            throw new EntityNotFoundException("Entidade com id " + pk + " não encontrada.");
        }
        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<E> findAll() {
        return em.createQuery("SELECT T FROM " + entityClass.getSimpleName() + " T").getResultList();
    }
    
    @Override
    @SuppressWarnings("unchecked")    
    public List<E> findByQuery(String query) {
        return em.createQuery(query, entityClass).getResultList();
    }

    @SuppressWarnings("unchecked")
    public static <E> E unproxyEntity(E entity) {
        if (entity == null) {
            return null;
        }
        if (entity instanceof HibernateProxy) {
            Hibernate.initialize(entity);
            entity = (E) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
        }
        return entity;
    }

}
