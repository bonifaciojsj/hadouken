drop table if exists ajusteestoque;
drop table if exists entrada_produto;
drop table if exists saida_produto;
drop table if exists saida;
drop table if exists entrada;
drop table if exists produto;
drop table if exists almoxarifado;
drop table if exists subtipoproduto;
drop table if exists tipoproduto;
drop table if exists cliente;
drop table if exists fornecedor;
drop table if exists usuario;

CREATE TABLE usuario (
id bigint not null primary key auto_increment,
username varchar(255) not null,
password varchar(255) not null,
nome varchar(255) not null,
sobrenome varchar(255),
email varchar(255) not null,
ativo boolean not null
);

insert into usuario values(1, 'webmaster', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Webmaster', 'Webmaster', 'bonifacio.jsj@gmail.com', true);

create table cliente(
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
nome Varchar(255) not null,
sobrenome varchar(255) not null,
tipopessoa varchar(1),
cpf varchar(20),
cnpj varchar(30),
email varchar(255),
celular varchar(100),
fone varchar(100),
datanascimento date,
estado varchar(2),
cidade varchar(255),
bairro varchar(255),
logradouro varchar(1000),
numero varchar(100),
ativo boolean not null,
usuariocadastro_id bigint DEFAULT NULL,
CONSTRAINT fk_cliente_usuario foreign key (usuariocadastro_id) references usuario(id)
);

create table fornecedor(
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
razaosocial Varchar(255) not null,
cnpj varchar(30),
email varchar(255),
fone varchar(100),
estado varchar(2),
cidade varchar(255),
bairro varchar(255),
logradouro varchar(1000),
numero varchar(20),
ativo boolean not null,
usuariocadastro_id bigint DEFAULT NULL,
CONSTRAINT fk_fornecedor_usuario foreign key (usuariocadastro_id) references usuario(id)
);



CREATE TABLE almoxarifado (
id bigint not null primary key auto_increment,
nome varchar(255) not null,
ativo boolean not null,
usuariocadastro_id bigint default null,
CONSTRAINT fk_almoxarifado_usuario foreign key (usuariocadastro_id) references usuario(id)
);

CREATE TABLE tipoproduto (
id bigint not null primary key auto_increment,
nome varchar(255) not null,
ativo boolean not null,
usuariocadastro_id bigint default null,
CONSTRAINT fk_tipoproduto_usuario foreign key (usuariocadastro_id) references usuario(id)
);

CREATE TABLE subtipoproduto (
id bigint not null primary key auto_increment,
nome varchar(255) not null,
ativo boolean not null,
usuariocadastro_id bigint default null,
tipoproduto_id bigint default null,
CONSTRAINT fk_subtipoproduto_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_subtipoproduto_tipoproduto foreign key (tipoproduto_id) references tipoproduto(id)
);

create table produto(
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
nome varchar(255) not null,
codigobarras varchar(255),
estoque integer,
estoqueMinimo integer not null,
grupo varchar(2) not null,
ativo boolean not null,
usuariocadastro_id bigint default null,
subtipoproduto_id bigint default null,
CONSTRAINT fk_produto_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_produto_subtipoproduto foreign key (subtipoproduto_id) references subtipoproduto(id)
);

create table entrada (
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
fornecedor_id bigint,
observacao varchar(1000),
status varchar(1),
datacadastro date,
dataconclusao date,
origem varchar(2) not null default 0,
ativo boolean not null,
almoxarifado_id bigint not null,
usuariocadastro_id bigint default null,
CONSTRAINT fk_entrada_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_entrada_fornecedor foreign key (fornecedor_id) references fornecedor(id),
CONSTRAINT fk_entrada_almoxarifado foreign key (almoxarifado_id) references almoxarifado(id)
);

create table saida (
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
cliente_id bigint,
observacao varchar(1000),
status varchar(1),
datacadastro date,
dataconclusao date,
origem varchar(2) not null default 0,
ativo boolean not null,
almoxarifado_id bigint not null,
usuariocadastro_id bigint default null,
CONSTRAINT fk_saida_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_saida_cliente foreign key (cliente_id) references cliente(id),
CONSTRAINT fk_saida_almoxarifado foreign key (almoxarifado_id) references almoxarifado(id)
);

create table entrada_produto (
entrada_id bigint default null,
produto_id bigint default null,
quantidade int not null default 1,
CONSTRAINT fk_entrada_produto_entrada foreign key (entrada_id) references entrada(id),
CONSTRAINT fk_entrada_produto_produto foreign key (produto_id) references produto(id)
);

create table saida_produto (
saida_id bigint default null,
produto_id bigint default null,
quantidade int not null default 1,
CONSTRAINT fk_saida_produto_saida foreign key (saida_id) references saida(id),
CONSTRAINT fk_saida_produto_produto foreign key (produto_id) references produto(id)
);

create table ajusteestoque (
id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
saida_id bigint not null,
entrada_id bigint not null,
finalizado boolean default 0,
datacadastro date,
dataconclusao date,
almoxarifadosaida_id bigint not null,
almoxarifadoentrada_id bigint not null,
usuariocadastro_id bigint not null,
CONSTRAINT fk_ajusteestoque_saida foreign key (saida_id) references saida(id),
CONSTRAINT fk_ajusteestoque_entrada foreign key (entrada_id) references entrada(id),
CONSTRAINT fk_ajusteestoque_usuario foreign key (usuariocadastro_id) references usuario(id),
CONSTRAINT fk_ajusteestoque_almoxarifadosaida foreign key (almoxarifadosaida_id) references almoxarifado(id),
CONSTRAINT fk_ajusteestoque_almoxarifadoentrada foreign key (almoxarifadoentrada_id) references almoxarifado(id)
);